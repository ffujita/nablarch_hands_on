<%@page import="com.nablarch.example.app.web.common.code.ProjectClass"%>
<%@page import="com.nablarch.example.app.web.common.code.ProjectType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="n" uri="http://tis.co.jp/nablarch" %>
<%@ page session="false" %>

<!DOCTYPE html>
<html>
    <head>
        <%-- javascript --%>
        <n:script type="text/javascript" src="/javascripts/lib/jquery-1.11.2.min.js"></n:script>
        <n:script type="text/javascript" src="/javascripts/projectInput.js"></n:script>
        <n:script type="text/javascript" src="/javascripts/clientList.js"></n:script>
        <title>プロジェクト登録画面</title>
    </head>

    <body>
        <div class="mainContents">
            <n:include path="/WEB-INF/view/common/menu.jsp" />
            <n:include path="/WEB-INF/view/common/header.jsp" />
        </div>
        <section>
            <%--TODO④ ウィンドウスコープの設定を記載する --%>
            <n:form useToken="true">
                <div class="title-nav">
                    <span class="page-title">プロジェクト登録画面</span>
                </div>
                <div class="message-area margin-top">
                    <n:errors filter="global" cssClass="message-error"/>
                </div>
                <h2 class="font-group">
                    プロジェクト情報
                </h2>
                <table class="table">
                    <tbody>
                        <tr>
                            <th class="width-250 required">
                                プロジェクト名
                            </th>
                            <td>
                                <div class="form-group">
                                    <n:write name="form.projectName"/>
                                    <n:error errorCss="message-error" name="form.projectName" />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="title-nav page-footer">
                    <div class="button-nav">
                        <n:forInputPage>
                            <div class="button-block real-button-block">
                                <n:button uri="/action/trainingProject/create" cssClass="btn btn-raised
                                btn-success">登録</n:button>
                            </div>
                            <div class="button-block link-button-block">
                                <n:button uri="/action/trainingProject/top" cssClass="btn btn-raised btn-default">入力へ戻る</n:button>
                            </div>
                        </n:forInputPage>
                        <n:forConfirmationPage>
                            <n:button uri="/action/trainingProject/top" cssClass="btn btn-raised btn-default">入力へ戻る</n:button>
                            <n:button uri="/action/trainingProject/create" cssClass="btn btn-raised btn-success" allowDoubleSubmission="false">確定</n:button>
                        </n:forConfirmationPage>
                    </div>
                </div>
            </n:form>
        </section>
        <n:include path="/WEB-INF/view/common/footer.jsp" />
        <n:forInputPage>
            <n:script type="text/javascript">
                $(function(){
                    setListUrlTo("topBackLink");
                    setListUrlTo("bottomBackLink");
                });
                $.material.init()
            </n:script>
        </n:forInputPage>

        <%-- 顧客検索 --%>
        <n:include path="/WEB-INF/view/client/index.jsp" />
    </body>
</html>
