package com.nablarch.example.app.web.action;

import com.nablarch.example.app.web.form.TrainingProjectForm;
import nablarch.core.db.statement.ParameterizedSqlPStatement;
import nablarch.core.db.support.DbAccessSupport;
import nablarch.core.message.ApplicationException;
import nablarch.core.message.Message;
import nablarch.core.message.MessageLevel;
import nablarch.core.message.MessageUtil;
import nablarch.core.validation.ValidationContext;
import nablarch.core.validation.ValidationUtil;
import nablarch.fw.ExecutionContext;
import nablarch.fw.web.HttpRequest;
import nablarch.fw.web.HttpResponse;
import nablarch.fw.web.interceptor.OnError;

public class TrainingProjectAction extends DbAccessSupport {


    /**
     * プロジェクト登録初期画面を表示。
     *
     * @param request HTTPリクエスト
     * @param context 実行コンテキスト
     * @return HTTPレスポンス
     */
    public HttpResponse top(HttpRequest request, ExecutionContext context) {
        return new HttpResponse("/WEB-INF/view/project/trainingCreate.jsp");
    }

    /**
     * 登録情報確認画面を表示。
     *
     * @param request HTTPリクエスト
     * @param context 実行コンテキスト
     * @return HTTPレスポンス
     */
    //TODO ①abortIfInvalid()で発生するエラーをハンドリングして、trainingCreate.jspを表示させる。ステータスコードは400
    public HttpResponse confirmOfCreate(HttpRequest request, ExecutionContext context) {
        ValidationContext<TrainingProjectForm> validationContext = ValidationUtil.validateAndConvertRequest("form", TrainingProjectForm.class, request, "confirmation");

        if (!validationContext.isValid()) {
            //TODO ② エラーメッセージを修正して画面に表示させる(メッセージＩＤはM999999999、出したい文言は「プロジェクト名は64文字より少ない文字数で入力してね。」)
            //参考\nablarch-example-web\src\main\resources\messages.properties
            //エラーメッセージの表示処理はtrainingCreate.jspの43行目に記述済です
            //TODO ③自分でメッセージを指定せず、validationContext.abortIfInvalid();　を利用する
            //        validationContext.abortIfInvalid();
            throw new ApplicationException();
        }
        TrainingProjectForm form = validationContext.createObject();
        context.setRequestScopedVar("form", form);
        return new HttpResponse("/WEB-INF/view/project/confirmOfTrainingCreate2.jsp");
    }
}