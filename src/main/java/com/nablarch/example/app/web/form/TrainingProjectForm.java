package com.nablarch.example.app.web.form;

import nablarch.core.validation.PropertyName;
import nablarch.core.validation.ValidateFor;
import nablarch.core.validation.ValidationContext;
import nablarch.core.validation.ValidationUtil;
import nablarch.core.validation.validator.Length;
import nablarch.core.validation.validator.unicode.SystemChar;

import java.io.Serializable;
import java.util.Map;

public class TrainingProjectForm implements Serializable {

    /** シリアルバージョンUID */
    private static final long serialVersionUID = 1L;

    /** プロジェクト名 */
    private String projectName;

    /**
     * コンストラクタ。
     *
     * @param params このクラスに設定するパラメータを持つMap
     */
    public TrainingProjectForm(Map<String, Object> params) {
        projectName = (String) params.get("projectName");
    }

    /**
     * プロジェクト名を取得する。
     *
     * @return プロジェクト名
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * プロジェクト名を設定する。
     *
     * @param projectName 設定するプロジェクト名
     */
    @Length(max = 64)
    @PropertyName("プロジェクト名")
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * 精査対象プロパティ。
     */
    private static final String[] KAKUNIN_GAMEN_TARGET = new String[]{"projectName"};

    /**
     * 登録確認画面表示イベント時に実施するバリデーション。
     *
     * @param context バリデーションの実行に必要なコンテキスト
     */
    @ValidateFor("confirmation")
    public static void validateForSearchSelectItem(ValidationContext<TrainingProjectForm> context) {
        ValidationUtil.validate(context, KAKUNIN_GAMEN_TARGET);
    }
}
